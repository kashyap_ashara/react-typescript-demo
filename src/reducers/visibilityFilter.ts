import { VisibilityFilters } from '../actions'

const visibilityFilter = (state: any = VisibilityFilters.SHOW_ALL, action: any) => {
    if (action.type=== 'SET_VISIBILITY_FILTER')
        return action.filter
    else
        return state
}

export default visibilityFilter