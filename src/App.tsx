import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import AddTodo from './components/AddTodo';
import Footer from './components/Footer';
import VisibleTodoList from './containers/VisibleTodoList';
import CounterExample from './CounterExample';
import './App.css';

function TodoApp(){
  return(
    <>
    <AddTodo />
    <VisibleTodoList />
    <Footer />
    </>
  )
}

function App() {
  return (
    <div>
      <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/counter">Counter</Link>
              </li>
            </ul>
          </nav>

          <Route path="/" exact component={TodoApp} />
          <Route path="/counter" component={CounterExample} />
        </div>
      </Router>
    </div>
  );
}

export default App;
